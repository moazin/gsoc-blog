## Path Description to Polyline

Livarot has a `Path` class which is initialized by creating path descriptions. We can think of these as commands to a drawing program and they are very analogous to an SVG path description. These can consist of commands such as `MoveTo`, `LineTo`, `CubicTo` and `ArcTo`. These commands are just stored in an array named `descr_cmd`. 

The next step is to call one of the functions `Convert`, `ConvertEvenLines` or `ConvertWithBackData` to create line segments which approximate the given path description. Each one of these take a threshold argument that will dictate how close the approximation is to the actual path descriptions.   Now you might be thinking why have three functions for conversion to line segments. Let me explain each one:

1. **Convert:** It creates line segments which approximate the given path descriptions. Line segments don't get split up, since a line segment approximates itself with zero error. In other words, a line segment remains a line segment.
2. **ConvertEvenLines: ** It is same as **Convert** except that lines also get split up. Say you have a 5 units long line and your threshold is 1, it would be split into 5 line segments. 
3. **ConvertWithBackData:** It is the same as **Convert** except that each point of the the approximating polyline holds a `t` value that tells us the time value that point corresponds to in the original path description. `t` being 0 indicates that the point is at the beginning of the path description. `t` being 1 indicates that the point is at the end of the path description. `t` being 0.5 indicates the point being halfway in the original path description.

The question of when each one of these would be suited to use is yet to be answered. For now I have observed that **ConvertEvenLines** is used when using the simplifying algorithm. 

Now let's explore how the actual approximation process happens. 

1. **MoveTo:** A point is simply added with a flag that indicates it's coming from a `MoveTo` instruction.
2. **LineTo:** If it's not `ConvertEvenLines` then the final point of the line segment is simply added to the list of points otherwise the line is split into smaller line segments of a size less than or equal to the threshold and each one of those points are added in the list of points.
3. **CubicTo:** This one is pretty interesting. It calls a recursive function `RecCubicTo` that checks if a line segment from the start point to the end point approximates the underlying cubic bezier well, if yes, proceed with that approximation, otherwise, split the bezier curve into two cubic bezier curves. One on the left and another on the right. The "halfway" point of the original curve is where the split happens. Then the same function is called on both the segments. 
4. **ArcTo: ** This one has a lot of Elliptical Arc evaluation stuff that I haven't been able to understand exactly since the technique isn't documented. But it basically takes the arc data that you'd have in an SVG path description and calculates the center and the start and end angle. Once that's done, a `max_angle` is calculated. I am not sure, but my immediate guess is, it gives the largest possible angle, sweeping which creates an arc that is smaller than or equal to the threshold. Basically it's the largest change in angle that won't break your threshold. Once that has been calculated, the `AngleInterval` from the start angle to the end angle is split into sectors (by dividing it with `max_angle`). The resultant points are added in the points list. Basically, this is just getting the arc points at specific `t` values such that the threshold is maintained.



