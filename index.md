## Exploring Livarot

Hi everyone. This summer I am going to be spending a lot of time working on my GSoC project "Path Library Improvement" with Inkscape.  Inkscape relies on a very old but powerful library called *livarot*. It was written in 2003 and works really well, however, it's very hard to understand the algorithms in it and the code quality is very low.

My goal in this project is to understand those algorithms and redesign them in a better way, possibly inside *lib2geom* which is Inkscape's own 2D geometry engine. I'm making this blog so I can share my findings. Writing about what I learn is not only going to bring me clarity, it will also be pretty useful for anyone who wants look back in future.

The following is an index to all the posts/pages that I write.

1. Going from Path Descriptions to a Polyline.